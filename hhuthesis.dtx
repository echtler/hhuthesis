% \iffalse meta-comment
%
% Copyright (C) 2023 by Daniel Echtler
% -----------------------------------
%
% This file may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either
% version 1.3 of this license or (at your option) any later
% version.  The latest version of this license is in:
%
%    http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of
% LaTeX version 2005/12/01 or later.
%
% \fi
%
% \iffalse
%<package>\NeedsTeXFormat{LaTeX2e}[2005/12/01]
%<package>\ProvidesPackage{hhuthesis}
%<package>    [2023/06/13 v1.0 phd-thesis template for the hhu]
%
%<*driver>
\documentclass{ltxdoc}

\let\KOMAClassName\empty  % So we can actually load hhuthesis
\usepackage{hhuthesis}

\EnableCrossrefs
\CodelineIndex
\RecordChanges

\usepackage{booktabs}
\usepackage{scrlogo}
\usepackage{verbatim}
\usepackage{hyperref}

\begin{document}
  \DocInput{hhuthesis.dtx}
\end{document}
%</driver>
% \fi
%
% \CheckSum{577}
%
% \CharacterTable
%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%   Digits        \0\1\2\3\4\5\6\7\8\9
%   Exclamation   \!     Double quote  \"     Hash (number) \#
%   Dollar        \$     Percent       \%     Ampersand     \&
%   Acute accent  \'     Left paren    \(     Right paren   \)
%   Asterisk      \*     Plus          \+     Comma         \,
%   Minus         \-     Point         \.     Solidus       \/
%   Colon         \:     Semicolon     \;     Less than     \<
%   Equals        \=     Greater than  \>     Question mark \?
%   Commercial at \@     Left bracket  \[     Backslash     \\
%   Right bracket \]     Circumflex    \^     Underscore    \_
%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%   Right brace   \}     Tilde         \~}
%
% \changes{v1.0}{2023/06/13}{Initial version}
%
% % \DoNotIndex{}
% 
% \GetFileInfo{hhuthesis.sty}
%
% \title{The \textsf{hhuthesis} package\thanks{This document
%   corresponds to \textsf{hhuthesis}~\fileversion,
%   dated \filedate.}}
% \author{Daniel Echtler \\ \texttt{daniel.echtler@mailbox.org}}
%
% \maketitle
%
% \begin{abstract}
%   This package provides a redefinition of |\maketitle| to create a titlepage
%   for a PhD-thesis at the Heinrich-Heine-Universität Düsseldorf. Moreover,
%   it defines commands for creating a declaration of academic honesty.
% \end{abstract}
%
% \section{Introduction}
%
%   The Doctorate Regulations of the Math.-Nat.~Faculty of the/
%   Heinrich-Heine-Universität Düsseldorf\footnote{\url{https://www.math-nat-fak.hhu.de/promotion/promotionsordnung-und-formulare-zur-promotion}}
%   require your titlepage to have a certian format. This packages redefines
%   |\maketitle| for an easy creation of this titlepage.
%
%   Additonally, the regulation requires you to include a declaration of academic
%   honesty with a precise wording. This can simply be created by using one of
%   the provided commands |\statement| or |\statement*|.
%
% \paragraph{Requirements}
% This package makes heavy use of some features provided by \KOMAScript.
% Thus it can only be used with one of the |scr*|-classes (i.e., |scrartcl|,
% |scrrprt| or |scrbook|, it is definitley recommended to use |scrbook|).
%
% \section{Usage}
% \subsection{Package options}
%   This package has only two options:
%   
%   \DescribeMacro{sansserif}
%   The option |sansserif| will simply changes the font of the titlepage to
%   be sansserif.
%   
%   \DescribeMacro{nodate}
%   By default the signature line includes a printed location and date. This can
%   be disabled by using the |nodate| option.
%
% \subsection{Title creation}
%   In order for |\maketitle| to typeset the actual title of the thesis one can
%   to use the usual storing commands |\title|, |\subtitle|, |\author| and
%   |\date|. Additional the package provides some more storing commands.
% 
%   \DescribeMacro{\birthplace}
%   \DescribeMacro{\defencedate}
%   \DescribeMacro{\faculty}
%   \DescribeMacro{\institute}
%   \DescribeMacro{\location}
%   \DescribeMacro{\referee}
%   \DescribeMacro{\university}
%   All of these commands are used to store the thing implied by their names.
%   Some of these have predefinde default values. These can be found in
%   \autoref{tab:defaults}.
%
%   In order to specify more than one referee one can either seperate multiple
%   referees by using |\and| inside the call of |\referee| or simply call
%   |\referee| multiple times.
%
%   Although this package redefines |\maketitle|, one can still use the storing
%   commands |\extratitle| and |\dedication| provided by \KOMAScript.
%   
%
%   \bigskip
%   As this package is only intended for use at the Math.-Nat.\ Faculty of the
%   Heinrich-Heine-Universität Düsseldorf the commands |\faculty| and
%   |\university| should not be used. In case they are still used one should
%   keep in mind that by default the set values only appear in the combination
%   \begin{quote}
%     \dots der \meta{content of {\normalfont\texttt{\textbackslash faculty}}} der \meta{content of {\normalfont\texttt{\textbackslash university}}}.
%   \end{quote}
%   Thus one should be careful with the value of |\faculty|. For example the
%   using the actual faculty name
%   \begin{quote}
%     |\faculty{Mathematisch-Naturwissenschaftliche Fakultät}|
%   \end{quote}
%   would result in
%   \begin{quote}
%     \dots der Mathematisch-Naturwissenschaftliche Fakultät der Heinrich-Heine-Universität Düsseldorf,
%   \end{quote}
%   which is grammatically not corret.
%
%   For further customization options of the titlepage one should have a look
%   at the actual implementation below.
%
%   \begin{table}[hb]
%     \begin{center}
%       \begin{tabular}{l l}
%         Command       & Default value                                           \\\midrule
%         |\date|       & \meta{current month (in german)}, \meta{current year}   \\
%         |\faculty|    & Mathematisch-Naturwissenschaftlichen Fakutlät           \\
%         |\institute|  & Mathematischen Institut                                 \\
%         |\location|   & Düsseldorf                                              \\
%         |\university| & Heinrich-Heine-Universität Düsseldorf
%       \end{tabular}
%       \caption{Default values applied to storing command}
%       \label{tab:defaults}
%     \end{center}
%   \end{table}
%
% \subsection{Statement of academic honesty}
%   \DescribeMacro{\statement}
%   \DescribeMacro{\statement*}
%   For the statement of academic honesty the package provides two commands
%   |\statement| and |\statement*|. The started version will output a statement
%   without pagenumber which does not appear in the table of contents.
%
%   For correct hyphenation of the statement the language package |babel|
%   should be loaded.
%
%   Again, for further customization we refere to the implementation (or the
%   example section) below.
%
% \clearpage
% \section{Example}
%    Here is a simple, slightly extensive example on how to use this package:
%    \verbatiminput{example.tex}
% 
% \StopEventually{}
%
% \section{Implementation}
%   This package should only be used with one of the \KOMAScript-classes. Thus
%   we check for the existence of |\KOMAClassName|. The second 
%    \begin{macrocode}
\@ifundefined{KOMAClassName}{%
  \PackageError{hhuthesis}{You have to use a KOMA-class to use this package!}{}
  \endinput
}
%    \end{macrocode}
%   If the command is empty (which should only happen for this |.dtx|-file) we
%   still want to end the input.
%    \begin{macrocode}
\ifx\KOMAClassName\@empty
  \endinput
\fi
%    \end{macrocode}
% \subsection{Option definitions}
%   \begin{macro}{sansserif}
%     The option |sansserif| simply adds |\sffamily| to all relevant fonts.
%    \begin{macrocode}
\DeclareOption{sansserif}{%
  \AtBeginDocument{%
    \addtokomafont{subject}{\sffamily}
    \addtokomafont{author}{\sffamily}
    \addtokomafont{date}{\sffamily}
    \addtokomafont{titleback}{\sffamily}
  }
}
%    \end{macrocode}
%   \end{macro}
%   \begin{macro}{nodate}
%     For the option |nodate| we add a new conditional and change its value if
%     the option is chosen.
%    \begin{macrocode}
\newif\if@statement@date
\@statement@datetrue

\DeclareOption{nodate}{%
  \@statement@datefalse
}
%    \end{macrocode}
%   \end{macro}
%     Since these are the only options we add a default processing of all other
%     values passed to the package. Moreover, we can also execute the chosen
%     options.
%    \begin{macrocode}
\DeclareOption*{\PackageWarning{hhuthesis}{Unknown '\CurrentOption'}}
\ProcessOptions\relax
%    \end{macrocode}
% \subsection{Titlepage redefinition}
%   First we create a new font and some empty variables for later use.
%    \begin{macrocode}
\newkomafont{titleback}{}

\let\@birthplace\@empty
\let\@defencedate\@empty
\let\@faculty\@empty
\let\@institute\@empty
\let\@location\@empty
\let\@referee\@empty
\let\@university\@empty
%    \end{macrocode}
%   \begin{macro}{\birthplace}
%   \begin{macro}{\defencedate}
%   \begin{macro}{\faculty}
%   \begin{macro}{\institute}
%   \begin{macro}{\location}
%   \begin{macro}{\referee}
%   \begin{macro}{\university}
%     Now we add commands to overwrite the above variables. Only the definition
%     of |\referee| is more creative: It appends to the (previously empty)
%     variable in order to allow it to be called multiple times.
%    \begin{macrocode}
\newcommand\birthplace[1]{\def\@birthplace{#1}}
\newcommand\defencedate[1]{\def\@defencedate{#1}}
\newcommand\faculty[1]{\def\@faculty{#1}}
\newcommand\institute[1]{\def\@institute{#1}}
\newcommand\location[1]{\def\@location{#1}}
\newcommand\referee[1]{\g@addto@macro\@referee{\item #1}}
\newcommand\university[1]{\def\@university{#1}}
%    \end{macrocode}
%   \end{macro}
%   \end{macro}
%   \end{macro}
%   \end{macro}
%   \end{macro}
%   \end{macro}
%   \end{macro}
%   To get the desired outcome we set some default values.
%    \begin{macrocode}
\RequirePackage[ngerman]{datetime}
\date{\monthnamengerman{} \the\year}

\location{Düsseldorf}
\faculty{Mathematisch-Naturwissenschaftlichen Fakultät}
\university{Heinrich-Heine-Universität Düsseldorf}
\institute{Mathematischen Institut}

\subject{%
  Inaugural-Dissertation\\[3em]
  zur Erlangung des Doktorgrades\\
  der \@faculty\\
  der \@university
}

\uppertitleback{%
  aus dem \@institute\par
  der \@university
}

\lowertitleback{%
  Gedruckt mit der Genehmigung der\par
  \@faculty\ der\par
  \@university\par
  \vskip 2em

  \ifx\@referee\@empty
    \PackageWarning{hhuthesis}{No referee(s) given}
  \else
    Berichterstatter:
    \begin{enumerate}%[label=\alph*.]
      \def\labelenumi{\arabic{enumi}.}
      \let\and\item
      \@referee
    \end{enumerate}
    \vskip 1em
  \fi

  Tag der mündlichen Prüfung:
  {\ifx\@defencedate\@empty
    \PackageWarning{hhuthesis}{No defence date given}
  \else
    \@defencedate
  \fi}
}
%    \end{macrocode}
%   The usual font size of the subject is to small.
%    \begin{macrocode}
\setkomafont{subject}{\normalfont\Large}
%    \end{macrocode}
%   \begin{macro}{\maketitle}
%     The (re)definition of |\maketitle| is mostly copied from the source of 
%     |scrbook|.
%    \begin{macrocode}
\renewcommand*\maketitle[1][1]{%
  \begin{titlepage}
    \setcounter{page}{%
      #1%
    }%
    \if@titlepageiscoverpage
      \edef\titlepage@restore{%
        \noexpand\endgroup
        \noexpand\global\noexpand\@colht\the\@colht
        \noexpand\global\noexpand\@colroom\the\@colroom
        \noexpand\global\vsize\the\vsize
        \noexpand\global\noexpand\@titlepageiscoverpagefalse
        \noexpand\let\noexpand\titlepage@restore\noexpand\relax
      }%
      \begingroup
      \topmargin=\dimexpr \coverpagetopmargin-1in\relax
      \oddsidemargin=\dimexpr \coverpageleftmargin-1in\relax
      \evensidemargin=\dimexpr \coverpageleftmargin-1in\relax
      \textwidth=\dimexpr
      \paperwidth-\coverpageleftmargin-\coverpagerightmargin\relax
      \textheight=\dimexpr
      \paperheight-\coverpagetopmargin-\coverpagebottommargin\relax
      \headheight=0pt
      \headsep=0pt
      \footskip=\baselineskip
      \@colht=\textheight
      \@colroom=\textheight
      \vsize=\textheight
      \columnwidth=\textwidth
      \hsize=\columnwidth
      \linewidth=\hsize
    \else
      \let\titlepage@restore\relax
    \fi
    \ifx\@extratitle\@empty
      \ifx\@frontispiece\@empty
      \else
        \if@twoside\mbox{}\next@tpage\fi
        \noindent\@frontispiece\next@tdpage
      \fi
    \else
      \noindent\@extratitle
      \ifx\@frontispiece\@empty
      \else
        \next@tpage
        \noindent\@frontispiece
      \fi
      \next@tdpage
    \fi
    \setparsizes{\z@}{\z@}{\z@\@plus 1fil}\par@updaterelative
%    \end{macrocode}
%     The actual title page.
%    \begin{macrocode}
    \null\vfill
    \begin{center}
      {\usekomafont{title}{\huge \@title\par}}%
      \vskip 1em
      {\ifx\@subtitle\@empty\else\usekomafont{subtitle}{\@subtitle\par}\fi}%
      \vfill
      {\ifx\@subject\@empty\else
        \usekomafont{subject}{%
          \@subject\par
          \vskip 2em
        }%
      \fi}
      {%
        \usekomafont{author}{%
          vorgelegt von\par
          \vskip 1em
          \textbf{\@author}\par
          \ifx\@birthplace\@empty\else
            aus \@birthplace\par
          \fi
        }%
      }%
      \vskip 6em
      {\usekomafont{date}{\@location, \@date \par}}
    \end{center}\par
    \vfill\null
%    \end{macrocode}
%     The back page of the title
%    \begin{macrocode}
    \@tempswatrue
    \ifx\@uppertitleback\@empty\ifx\@lowertitleback\@empty
      \@tempswafalse
    \fi\fi
    \if@tempswa
      \next@tpage
      \begin{minipage}[t]{\textwidth}
        \usekomafont{titleback}{\@uppertitleback}
      \end{minipage}\par
      \vfill
      \begin{minipage}[b]{\textwidth}
        \usekomafont{titleback}{\@lowertitleback}
      \end{minipage}\par
    \fi
%    \end{macrocode}
%     Back to the definition copied from |scrbook|.
%    \begin{macrocode}
    \ifx\@dedication\@empty
    \else
      \next@tdpage\null\vfill
      {\centering\usekomafont{dedication}{\@dedication \par}}%
      \vskip \z@ \@plus3fill
      \cleardoubleemptypage
    \fi
    \ifx\titlepage@restore\relax\else\clearpage\titlepage@restore\fi
  \end{titlepage}
  \let\maketitle\relax
  \let\@maketitle\relax
  \global\let\@title\@empty
  \global\let\@subtitle\@empty
  \global\let\@extratitle\@empty
  \global\let\@frontispiece\@empty
  \global\let\@titlehead\@empty
  \global\let\@subject\@empty
  \global\let\@publishers\@empty
  \global\let\@uppertitleback\@empty
  \global\let\@lowertitleback\@empty
  \global\let\@dedication\@empty
  \global\let\author\relax
  \global\let\date\relax
  \global\let\title\relax
  \global\let\subtitle\relax
  \global\let\extratitle\relax
  \global\let\frontispiece\relax
  \global\let\titlehead\relax
  \global\let\subject\relax
  \global\let\publishers\relax
  \global\let\uppertitleback\relax
  \global\let\lowertitleback\relax
  \global\let\dedication\relax
%    \end{macrocode}
%     Finally we set all of our custom variables empty again. Notice that we do
%     clear the variables |\@author|, |\@date| and |\@location| as we still need
%     them for the statement.
%    \begin{macrocode}
  \global\let\@referee\@empty
  \global\let\@defencedate\@empty
  \global\let\@birthplace\@empty
  \global\let\@faculty\@empty
  \global\let\@university\@empty
  \global\let\@institute\@empty
  %
  \global\let\referee\relax
  \global\let\location\relax
  \global\let\defencedate\relax
  \global\let\birthplace\relax
  \global\let\faculty\relax
  \global\let\university\relax
  \global\let\institute\relax
}
%    \end{macrocode}
%   \end{macro}
% \subsection{Statement definition}
%   \begin{macro}{\signaturewidth}
%   \begin{macro}{\locationwidth}
%   First we add some new lengths for the signature and location/date-line and set
%   them to the default value.
%    \begin{macrocode}
\newlength\signaturewidth
\newlength\locationwidth

\setlength\signaturewidth{5.5cm}
\setlength\locationwidth{5.5cm}
%    \end{macrocode}
%   \end{macro}
%   \end{macro}
%   \begin{macro}{\statmentname}
%   \begin{macro}{\statementtext}
%     Now we add some variables to contol the title and the actual text of the
%     statement.
%    \begin{macrocode}
\def\statementname{Eidestattliche Erklärung}

\newcommand\statementtext[1]{%
  \def\@statement@text{#1}
}

%    \end{macrocode}
%     Since the default text is in german we issue a warning if babel is not
%     loaded. For this we use |\ltx@ifpackageloaded| from |ltxcmds|, which can
%     be used in the main body in comparison to |\@ifpackageloaded|.
%    \begin{macrocode}
\RequirePackage{ltxcmds}
\statementtext{%
  \ltx@ifpackageloaded{babel}{\begin{otherlanguage}{ngerman}}{%
    \PackageWarning{hhuthesis}{Language package babel not loaded!\MessageBreak Using wrong hyphenation}
  }
    Ich versichere an Eides Statt, dass die Dissertation von mir selbstständig
    und ohne unzulässige fremde Hilfe unter Beachtung der \glqq{}Grundsätze zur
    Sicherung guter wissenschaftlicher Praxis an der Heinrich-Heine-Universität
    Düsseldorf\grqq{} erstellt worden ist.
  \ltx@ifpackageloaded{babel}{\end{otherlanguage}}{}
}
%    \end{macrocode}
%   \end{macro}
%   \end{macro}
%   \begin{macro}{\signatureline}
%     The macro |\signatureline| creates a simple signature line to be used
%     for the statement.
%    \begin{macrocode}
\newcommand\signatureline{%
  \noindent
  \setlength\tabcolsep{4pt}
  \begin{tabular}{l}
    \if@statement@date{\Large\@location, \@date}\fi
    \\\hline\hspace*\locationwidth\\[-.75\normalbaselineskip]
    Ort, Datum
  \end{tabular} \hfill
  \begin{tabular}{l}
    \\\hline\hspace*\signaturewidth\\[-.75\normalbaselineskip]
    \@author
  \end{tabular}%
}
%    \end{macrocode}
%   \end{macro}
%   \begin{macro}{\statement}
%   \begin{macro}{\statement*}
%     Finally we add the stared- and unstared-version of |\statement|.
%    \begin{macrocode}
\newcommand\statement{\@ifstar\statement@star\statement@nostar}

\newcommand\statement@star{%
  \addchap*{\statementname}
  \thispagestyle{empty}
  \@statement@text\\[1.7cm]
  \signatureline
  \clearpage
}

\newcommand\statement@nostar{%
  \addchap{\statementname}
  \@statement@text\\[1.7cm]
  \signatureline
  \clearpage
}

\endinput
%    \end{macrocode}
%   \end{macro}
%   \end{macro}
%
% \Finale
\endinput
