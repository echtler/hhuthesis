# The hhuthesis package

This package provides a redefinition of `\maketitle` to create a titlepage for
a PhD-thesis at the Heinrich-Heine-Universität Düsseldorf. Moreover, it defines
commands for creating a declaration of academic honesty.

## Installation

In order to generate the `hhuthesis.sty` file, simply run `latex hhuthesis.ins`.
Afterwards you can generate the documentation by running `pdflatex hhuthesis.dtx`.
Alternatively you can download the `hhuthesis.sty` file directly from the
[releases page](https://gitlab.com/echtler/hhuthesis/-/releases).
