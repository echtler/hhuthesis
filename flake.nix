{
  description = "A PhD-thesis template for the HHU";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
        packages = {
          default = with pkgs; stdenvNoCC.mkDerivation {
            pname = "latex-hhuthesis";
            version = "1.0";
            src = self;
          
            passthru = {
              tlDeps = [ texlive.datetime ];
              tlType = "run";
            };

            nativeBuildInputs = [ (pkgs.texlive.combine {
              inherit (pkgs.texlive) scheme-basic datetime booktabs koma-script hypdoc;
              })
            ];

            phases = [ "unpackPhase" "buildPhase" "installPhase" ];

            buildPhase = ''
              # Generate the style file
              latex hhuthesis.ins

              # Generate the documenation
              pdflatex hhuthesis.dtx
            '';
          
            installPhase = ''
              # Install the style file
              install -D -t $out/tex/latex/hhuthesis/ hhuthesis.sty

              # Install the documentation
              install -D -t $out/doc/latex/hhuthesis/ hhuthesis.pdf
            '';
          };
        };
      }
    );
}
